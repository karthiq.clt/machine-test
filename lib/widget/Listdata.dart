import 'package:Studentdetails/Bloc/Studentdetailsbloc.dart';
import 'package:Studentdetails/model/Studentmode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Listdata extends StatelessWidget {
  final List<Studentmodel> datas;
  final int index;
  Listdata(this.index, this.datas);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        BlocProvider.of<Studentbloc>(context).add(Fetchselectedstudent(
          datas.elementAt(index).age,
          datas.elementAt(index).name,
        ));
      },
      child: Card(
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.person),
            ),
            SizedBox(
              width: 10.0,
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 5),
                child: Text(
                  datas.elementAt(index).name,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 5),
                child: Text(
                  datas.elementAt(index).age.toString(),
                ),
              ),
            ])
          ],
        ),
      ),
    );
  }
}
