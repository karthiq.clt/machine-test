import 'package:Studentdetails/model/Studentmode.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StudentEvent {}

class FetchStudentdetails extends StudentEvent {
  List<Studentmodel> datas;
  FetchStudentdetails(this.datas);
}

class Showdataevent extends StudentEvent {}

class Fetchselectedstudent extends StudentEvent {
  int age;
  String name;
  Fetchselectedstudent(this.age, this.name);
}

class StudentState {}

class ShowQrcode extends StudentState {
  int age;
  String name;
  ShowQrcode(this.age, this.name);
  int get getage => age;
  String get getname => name;
}

class Showdata extends StudentState {
  List<Studentmodel> datas;
  Showdata(this.datas);
  List<Studentmodel> get getdata => datas;
}

class Initialstate extends StudentState {}

class Studentbloc extends Bloc<StudentEvent, StudentState> {
  Studentbloc() : super(Initialstate());
  @override
  Stream<StudentState> mapEventToState(StudentEvent event) async* {
    if (event is Fetchselectedstudent) {
      yield ShowQrcode(event.age, event.name);
    } else if (event is FetchStudentdetails) {
      yield Showdata(event.datas);
    } else if (event is Showdataevent) {
      yield Initialstate();
    }
  }
}
