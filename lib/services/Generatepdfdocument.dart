import 'dart:typed_data';
import 'package:Studentdetails/model/Studentmode.dart';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/widgets/font.dart';
import 'package:pdf/widgets/theme.dart';

Future<Uint8List> generateDocument(List<Studentmodel> datas) async {
  var myTheme = ThemeData.withFont(
    base: Font.ttf(await rootBundle.load("assets/OpenSans-Regular.ttf")),
    bold: Font.ttf(await rootBundle.load("assets/OpenSans-Bold.ttf")),
  );

  final pw.Document doc = pw.Document(
    theme: myTheme,
  );

  doc.addPage(pw.MultiPage(
      pageFormat:
          PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
      crossAxisAlignment: pw.CrossAxisAlignment.start,
      header: (pw.Context context) {
        if (context.pageNumber == 1) {
          return null;
        }
        return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            decoration: const pw.BoxDecoration(),
            child: pw.Text('Portable Document Format',
                style: pw.Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      build: (pw.Context context) => <pw.Widget>[
            pw.Table.fromTextArray(
                context: context,
                border: null,
                headerAlignment: pw.Alignment.centerLeft,
                data: <List<String>>[
                  <String>['Id', 'Name', 'Age'],
                  for (int i = 0; i < datas.length; i++)
                    <String>[
                      '${datas.elementAt(i).id.toString()}',
                      '${datas.elementAt(i).name}',
                      '${datas.elementAt(i).age.toString()}'
                    ],
                ]),
            pw.Paragraph(text: ""),
            pw.Padding(padding: const pw.EdgeInsets.all(10)),
          ]));
  return doc.save();
}
