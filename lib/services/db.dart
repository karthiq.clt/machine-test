import 'dart:async';
import 'package:Studentdetails/model/Studentmode.dart';
import 'package:sqflite/sqflite.dart';

abstract class DB {
  static Database _db;
  static int get _version => 1;
  static Future<void> init() async {
    if (_db != null) {
      return;
    }
    try {
      String _path = await getDatabasesPath() + 'example';
      _db = await openDatabase(_path, version: _version, onCreate: onCreate);
    } catch (ex) {
      print(ex);
    }
  }

  static void onCreate(Database db, int version) async => await db.execute(
      'CREATE TABLE Students(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)');

  static Future<List<Map<String, dynamic>>> query() async =>
      _db.query("Students");

  static Future<int> insert(Studentmodel model) async =>
      await _db.insert("Students", model.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
}
