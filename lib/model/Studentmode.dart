class Studentmodel {
  int id;
  String name;
  int age;

  Studentmodel({this.id, this.name, this.age});

  Studentmodel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    age = json['age'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['age'] = this.age;
    return data;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  static Studentmodel fromMap(Map<String, dynamic> map) {
    return Studentmodel(id: map['id'], name: map['name'], age: map['age']);
  }
}
