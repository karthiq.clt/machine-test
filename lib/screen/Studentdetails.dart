import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:Studentdetails/model/Studentmode.dart';
import 'package:Studentdetails/Bloc/Studentdetailsbloc.dart';
import 'package:Studentdetails/services/Generatepdfdocument.dart';
import 'package:Studentdetails/services/Db.dart';
import 'package:Studentdetails/widget/Listdata.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:permission_handler/permission_handler.dart';

import 'package:qr_flutter/qr_flutter.dart';

class Studentdetails extends StatefulWidget {
  @override
  _StudentdetailsState createState() => _StudentdetailsState();
}

class _StudentdetailsState extends State<Studentdetails> {
  List<Studentmodel> datas = [];
  var blocs;
  var name = "";
  int age;

  _loadJson() async {
    String data = await rootBundle.loadString('assets/Studentdetails.json');
    datas = (json.decode(data) as List)
        .map((dataitems) => Studentmodel.fromJson(dataitems))
        .toList();
    datas.forEach((element) async {
      await DB.insert(element);
    });
    List<Map<String, dynamic>> _results = await DB.query();
    datas = _results.map((item) => Studentmodel.fromMap(item)).toList();
    // print(datas.elementAt(0).name);
    blocs.add(FetchStudentdetails(datas));
  }

  Future<bool> _requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

/////// Code to as save pdf ///////
  Future<bool> _savePdf() async {
    Directory directory;
    try {
      if (Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          directory = await getExternalStorageDirectory();
          String newPath = "";
          print(directory);
          List<String> paths = directory.path.split("/");
          for (int x = 1; x < paths.length; x++) {
            String folder = paths[x];
            if (folder != "Android") {
              newPath += "/" + folder;
            } else {
              break;
            }
          }
          newPath = newPath + "/Student Details";
          directory = Directory(newPath);
        } else {
          return false;
        }
      } else {
        if (await _requestPermission(Permission.accessMediaLocation)) {
          directory = await getTemporaryDirectory();
        } else {
          return false;
        }
      }
      File saveFile = File(directory.path + "/Students.pdf");
      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }
      if (await directory.exists()) {
        var pdfdata = await generateDocument(datas);
        var file = File(saveFile.path);
        file.writeAsBytes(pdfdata);
        print(file.path);
        if (Platform.isIOS) {
          // await ImageGallerySaver.saveFile(saveFile.path,
          //     isReturnPathOfIOS: true);
        }
        return true;
      }
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    _loadJson();
  }

  @override
  Widget build(BuildContext context) {
    blocs = BlocProvider.of<Studentbloc>(context);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var saved = await _savePdf();

          Fluttertoast.showToast(
              msg: saved
                  ? "Pdf file is saved in the folder Student Details"
                  : "Please enable permission and try again",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          print(
            "File saved or not " + saved.toString(),
          );
        },
        child: Icon(
          Icons.file_download,
          color: Colors.white,
          size: 29,
        ),
        backgroundColor: Colors.black,
        elevation: 5,
        splashColor: Colors.grey,
      ),
      body: BlocConsumer<Studentbloc, StudentState>(
        builder: (context, state) {
          if (state is Initialstate) {
            return ListView(
              shrinkWrap: true,
              children: [
                Card(
                  child: name.compareTo("") == 0
                      ? Container(
                          height: 150,
                          child: Center(
                            child: Text("Please select a student"),
                          ),
                        )
                      : Column(
                          children: [
                            QrImage(
                              data: name + " " + age.toString(),
                              version: QrVersions.auto,
                              size: 150.0,
                            ),
                            Text(name),
                            Text(
                              age.toString(),
                            ),
                          ],
                        ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: datas.length,
                  itemBuilder: (BuildContext ctxt, int index) {
                    return Listdata(index, datas);
                  },
                ),
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is Showdata) {
            blocs.add(
              Showdataevent(),
            );
          }
          if (state is ShowQrcode) {
            name = state.name;
            age = state.age;
            blocs.add(
              Showdataevent(),
            );
          }
        },
      ),
    );
  }
}
